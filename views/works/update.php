<?php

use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model app\models\Post */

//$this->title = Yii::t('app', 'Create Post');

include \Yii::getAlias('@app/views/layouts/headers.php');
?>
<div class="post-create workform">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'id' => $model->id,
    ]) ?>

</div>
