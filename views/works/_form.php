<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="post-form ">
    <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
        <?php $form = ActiveForm::begin([
            'enableClientScript' => false
        ]); ?>

        <?= $form->errorSummary([$model]);?>
            <h1> Introduce tu escrito </h1>
         <div class="form-group field-works-title required">
            <label class="control-label" for="works-title">Título</label>
            <input type="text" id="works-title" class="form-control" name="Works[title]" maxlength="254" aria-required="true">

            <div class="help-block"></div>
        </div>   

        <div class="form-group field-works-content required">
            <label class="control-label" for="works-content">Contenido</label>
            <textarea rows="30" type="text" id="works-content" class="form-control" name="Works[content]" aria-required="true" ></textarea>
            <div class="help-block"></div>
        </div>


        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success float-right']) ?>
        </div>
        </div>
        <div class="col-3"></div>
    </div>
    
    <!-- $form->field($model, 'image_url')->textInput(['maxlength' => true])  

     $form->field($model, 'summary')->textarea(['rows' => 3]) 

     $form->field($model, 'content')->textarea(['rows' => 6, 'id'=>'content_editor']) 
     Html::a(Yii::t('app', 'Edit as markdown'), '#', ['id'=>'edit_markdown', 'class' => 'pull-right text text-danger'])
    <br/>
    
    <? if (\app\helpers\Helper::isAdmin()):
     $form->field($model, 'published_time')->textInput(['type' => 'datetime-local', 'value' => substr(date('c', $model->published_time), 0, 19)]) 
    
     $form->field($model, 'status')->checkbox()
    -->
    
 

    <?php ActiveForm::end(); ?>

</div>