<?php
/* @var $this \yii\web\View */
/* @var $content string */
use yii\helpers\Html;
?>
<?php 
include \Yii::getAlias('@app/views/layouts/headers.php');
$this->beginBlock('header') ?>
<header class="masthead " >
    <div class="overlay"></div>
    <div class="container ">
        <div class="row"> 
            <?=Html::encode($model->id)?>
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="post-heading ">
                    <h1><?=Html::encode($model->title)?></h1>
                    <h2 class="subheading"> <?= Html::encode($model->content)?> </h2>                  
                    
                </div>
            </div>
        </div>
    </div>
</header>
<?php $this->endBlock() ?>

<article>
    <div class="container workform">
        <!-- if ($model->status != Constant::STATUS_PUBLISHED || ($model->status == Constant::STATUS_PUBLISHED && $model->published_time > time())): 
        <div class="p-3 mb-2 bg-warning text-dark>('app', 'This post was not published.') <strong><em>Html::a(Yii::t('app', 'Update now'), ['/post/update', 'id' => $model->id])</em></strong></div>-->
        <!-- endif -->
        <div class="row">
            <div class="col-2">                
                <form action="#">
               <input class="btn btn-primary btn-create" type="submit" value="Editar" />
           </form>
            </div>
            <div class="col-lg-8 col-md-10 mx-auto">
                <h1><?=$model->title;?></h1><br/>
               <p> <?= $model->content ?> </p>
            </div>
            <div class="col-2">
                <div class="blog-social-share"><br/>       
                        <li class="fixed-social-list"><a href="#"><i class="social-icons-black fab fa-facebook fa-5x"></i></a></li><br>
                        <li class="fixed-social-list"><a href="#"><i class="social-icons-black fab fa-twitter-square fa-5x"></i></a></li><br>
                        <li class="fixed-social-list"><a href="#"><i class="social-icons-black fab fa-instagram fa-5x"></i></a></li><br>
                        <li class="fixed-social-list"><a href="#"><i class="social-icons-black fab fa-linkedin fa-5x"></i></a></li><br>
                        <li class="fixed-social-list"><a href="#"><i class="social-icons-black fab fa-google-plus-square fa-5x"></i></a></li>
                </div>
            </div>
        </div>
    </div>
</article>

<script>

</script>

