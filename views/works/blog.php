<?php  

use yii\helpers\Url;
use app\models\Works;

$this->title = 'Blog';
include \Yii::getAlias('@app/views/layouts/headers.php');


?>
<!--Slider-->
<div class="container slider_banner_info_top">
		
   <h2 class="heading text-center text-uppercase mt-5 mb-5">Blog</h2>
     
</div>

<div>
   <div class="row">
       <div class="col-2 text-center">
           <form action="<?= Url::toRoute('works/create');?>">
               <input class="btn btn-primary btn-create" type="submit" value="Crear algo " />
           </form>
       </div>
        <div class="col-7">
            <div class="row">
                <div class="col-1"></div>           
                <div class="col-9 fix-blog">                            
                    <img src="<?= \Yii::getAlias('@web/images/trap-rap-poesia.png') ?>" alt="" class="img-fluid fit-image" />
                    <h3 class="mt-3 my-2 text-capitalize">Rap, Trap y Poesía </h3>
                    <p class="mb-2">La primera pregunta es obvia y necesaria: ¿qué es el trap? Para Yung Beef, «el trap es cocaína y follar». Bien, de acuerdo. Pero, entonces, ¿qué hace Ernesto Castro, autor de este libro, que dice ser un joven abstemio, que no ha probado ninguna droga dura y hace años que no folla, tarareando a todas horas las canciones lascivas y politoxicómanas que solemos llamar «trap»? ¿Qué le lleva a dedicar tanto tiempo a escribir el primer libro amplio e imprescindible sobre el trap que se puede leer en castellano? Más aún: ¿qué le ha pasado a la juventud española para que un doctor en Filosofía de clase media tenga como referente cultural a una figura como Yung Beef? Sin duda alguna, y en muy poco tiempo, el trap no sólo se ha hecho un hueco en la industria musical y en los medios de comunicación main­stream, sino que ha obtenido una popularidad social apabullante. Pero hay algo más. </p>
                    <a href="#" class="text-capitalize">Read more <span class="fas fa-long-arrow-alt-right"></span></a>
                    <span class="author-post">Escrito por <a href="#">Ribaldhino</a></span>               

                </div>
                <div class="col-2"> </div>

                <div class="col-1"></div>           
                <div class="col-9 fix-blog">                            
                    <img src="<?= \Yii::getAlias('@web/images/crisis-ciclicas.png') ?>" alt="" class="img-fluid fit-image" />
                    <h3 class="mt-3 my-2 text-capitalize">Un hecho: Crisis Cíclicas </h3>
                    <p class="mb-2">Uno de los mayores problemas de los que adolecen nuestros juicios económicos es que tratamos de elucubrarlos a la luz de nuestra experiencia diaria. En ocasiones el resultado puede ser satisfactorio pero en otras puede resultar bastante catastrófico. Por ejemplo, por todos es sabido que al capitalismo lo mueve el consumo; basta con darse un paseo por la calle para darse cuenta: cuando las tiendas están a rebosar, se crea empleo, y cuando están vacías, se destruye. Sencillo, ¿no?</p>
                    <a href="#" class="text-capitalize">Read more <span class="fas fa-long-arrow-alt-right"></span></a>
                    <span class="author-post">Escrito por <a href="#">ZeuxisMaster</a></span>
                </div>
                <div class="col-2"> </div>            

                <div class="col-1"></div>           
                <div class="col-9 fix-blog">                            
                    <img src="<?= \Yii::getAlias('@web/images/protestantismo-vs-catolicismo.jpg') ?>" alt="" class="img-fluid fit-image" />
                    <h3 class="mt-3 my-2 text-capitalize">Protestantismo vs Catolicismo </h3>
                    <p class="mb-2">Los protestantes y los católicos son dos ramas distintas del cristianismo y que no son iguales. Pero, ¿cuáles son las diferencias fundamentales entre un católico y un protestante? La Iglesia Católica y la Iglesia Evangélica poseen un concepto distinto en lo que hace a la esencia de la Iglesia. La Iglesia Católica se considera a sí misma la única Iglesia verdadera y universal, bajo la dirección del Papa. </p>
                    <a href="#" class="text-capitalize">Read more <span class="fas fa-long-arrow-alt-right"></span></a>
                    <span class="author-post">Escrito por <a href="#">MollyChan</a></span>
                </div>
                <div class="col-2"> </div>
            </div>
            <?php 
            $i = 0;
                foreach ($dataProvider->models as $model) { 
                $i += 1;    ?>
                <div class="row">
                    <div class="col-1"></div>           
                    <div class="col-9 fix-blog">                            
                        <img src="<?= \Yii::getAlias('@web/images/crisis-ciclicas.png') ?>" alt="" class="img-fluid fit-image" />                   
                        <h3 class="mt-3 my-2 text-capitalize"><?php  echo $model->title; ?> </h3>
                        <p class="mb-2 blog-paragraph"><?php echo $model->content; ?> </p>
                        <a href="<?= Url::toRoute('works/view?id='.$i);?>" class="text-capitalize">Leer más <span class="fas fa-long-arrow-alt-right"></span></a>
                        <span class="author-post">Escrito por <a href="#">Anónimo</a></span>
                    </div>
                    <div class="col-2"> </div>  
                </div>
             <?php } ?>

        </div>
        <div class="col-3"><br/>
            <div class="category-div">
                <h5>Categorías </h5><br/>       
                <input class="category-label" type="checkbox" value="Economía"/><label class="category-label"for='sales'>Economía</label><br/>
                <input type="checkbox" value="Ciencia"/><label class="category-label" for='sales'>Ciencia</label><br/>
                <input type="checkbox" value="Política"/><label class="category-label" for='sales'>Política</label><br/>
                <input type="checkbox" value="Salud"/><label class="category-label" for='sales'>Salud</label><br/>
                <input type="checkbox" value="Tecnología"/><label class="category-label" for='sales'>Tecnología</label><br/>
                <input type="checkbox" value="Cultura"/><label class="category-label" for='sales'>Cultura</label><br/>
                <input type="checkbox" value="Empresa"/><label class="category-label" for='sales'>Emresa</label><br/>
                <input type="checkbox" value="Arte"/><label class="category-label" for='sales'>Arte</label><br/>
            </div>
        </div>
  </div>
</div>


<!-- js-scripts -->		

	<!-- js -->
	<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script> <!-- Necessary-JavaScript-File-For-Bootstrap --> 
	<!-- //js -->	
	
	<!-- Banner Responsive slider -->
	<script src="js/responsiveslides.min.js"></script>
	<script>
            
		// You can also use "$(window).load(function() {"
		$(function () {
			// Slideshow 3
			$("#slider3").responsiveSlides({
				auto: true,
				pager: false,
				nav: true,
				speed: 500,
				namespace: "callbacks",
				before: function () {
					$('.events').append("<li>before event fired.</li>");
				},
				after: function () {
					$('.events').append("<li>after event fired.</li>");
				}
			});

		});
	</script>
	<!-- // Banner Responsive slider -->
	
	<!-- stats -->
	<script src="js/jquery.waypoints.min.js"></script>
	<script src="js/jquery.countup.js"></script>
	<script>
		$('.counter').countUp();
	</script>
	<!-- //stats -->
	
	<!-- search-bar -->
	<script src="js/main.js"></script>
	<!-- //search-bar -->
	
	<!-- start-smoth-scrolling -->
	<script src="js/SmoothScroll.min.js"></script>
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
	</script>
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
	<!-- //here ends scrolling icon -->
	<!-- start-smoth-scrolling -->

	<!-- jQuery-Photo-filter-lightbox-Gallery-plugin -->
	<script type="text/javascript" src="js/jquery-1.7.2.js"></script>
	<script src="js/jquery.quicksand.js" type="text/javascript"></script>
	<script src="js/script.js" type="text/javascript"></script>
	<script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
	<!-- //jQuery-Photo-filter-lightbox-Gallery-plugin -->

<!-- //js-scripts -->

</body>
</html>