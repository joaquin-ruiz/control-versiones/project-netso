<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

$this->title = 'Conéctate';
include \Yii::getAlias('@app/views/layouts/headers.php');

?>
<div class="simple-vcenter-slider contact-slider">
    <div class="container">
           <div class="d-flex justify-content-center">
                   <div class="card">
                           <div class="card-header">
                                   <h3>Conéctate</h3>
                                   <div class="d-flex justify-content-end social_icon">
                                           <span><i class="fab fa-facebook-square"></i></span>
                                           <span><i class="fab fa-google-plus-square"></i></span>
                                           <span><i class="fab fa-twitter-square"></i></span>
                                   </div>
                           </div>
                           <div class="card-body">
                                   <form>
                                           <div class="input-group form-group">
                                                   <div class="input-group-prepend">
                                                           <span class="input-group-text form-spans"><i class="fas fa-user"></i></span>
                                                   </div>
                                                   <input type="text" class="form-control" placeholder="username">

                                           </div>
                                           <div class="input-group form-group">
                                                   <div class="input-group-prepend">
                                                           <span class="input-group-text form-spans"><i class="fas fa-key"></i></span>
                                                   </div>
                                                   <input type="password" class="form-control" placeholder="password">
                                           </div>
                                           <div class="row align-items-center remember">
                                                   <input type="checkbox">Recordarme
                                           </div>
                                           <div class="form-group">
                                                   <input type="submit" value="Entrar" class="btn float-right login_btn">
                                           </div>
                                   </form>
                           </div>
                           <div class="card-footer">
                                   <div class="d-flex justify-content-center links">
                                           ¿No tienes cuenta?<a href=""<?= Url::toRoute('site/register');?>">¡Regístrate!</a>
                                   </div>
                                   <div class="d-flex justify-content-center">
                                           <a href="#">¿Olvidaste tu contraseña?</a>
                                   </div>
                           </div>
                   </div>
           </div>
       </div>
   </div>
