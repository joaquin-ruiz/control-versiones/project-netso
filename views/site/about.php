<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

$this->title = 'Conoce NetSo';
include \Yii::getAlias('@app/views/layouts/headers.php');

?>
<section class="about py-5">
	<div class="container slider_banner_info_top">
		<h2 class="heading text-center text-uppercase mt-5 mb-5">NetSo</h2>
                <p class="h3 text-center mb-10"> Netso es una plataforma web destinada a fomentar el saber personal y la libertad de pensamiento. Se consolida como un apoyo virtual a la cultura universal y a la no censura, surgiendo ante el panorama de extremismos y polarización que la vida política ha creado en los continentes de Europa y Las Américas. NetSo se presenta como un refugio a la lucha cultural que enfocaría sus primeros andares en el estado-nación del Reino de España</p>
                <h3 class="text-center text-uppercase mb-5"><mark>Nuestro Equipo</mark></h3>
                    <div class="row about_grids mt-5 mb-10">
                            <div class="col-md-4">
                                    <img src="<?= \Yii::getAlias('@web/images/avatar-jodael.jpg') ?>" alt="Perfil de Jodael" class="rounded-circle img-fluid" />
                                    <h4 class="mt-3 my-2 text-capitalize">Jodael </h4>
                                    <p class="mb-2">Webmaster de Netso</p>
                            </div>
                            <div class="col-md-4 mt-md-0 mt-4">
                                    <img src="<?= \Yii::getAlias('@web/images/avatar-zalman.jpg') ?>" alt="Imagen vacante" class="rounded-circle img-fluid" />
                                    <h4 class="mt-3 my-2 text-capitalize">Zalman </h4>
                                    <p class="mb-2">Administrador de Netso</p>
                            </div>
                            <div class="col-md-4 mt-md-0 mt-4">
                                    <img src="<?= \Yii::getAlias('@web/images/avatar-tachamkla.jpg') ?>" alt="" class="rounded-circle img-fluid" />
                                    <h4 class="mt-3 my-2 text-capitalize">Tachamkla </h4>
                                    <p class="mb-2">Moderador de Netso</p>
                            </div>
                    </div>
                <h3 class="text-nowrap text-center text-uppercase mt-5 mb-5"><mark>Mejores Colaboradores</mark></h3>
                    <div class="row about_grids mt-5">
                            <div class="col-md-4">
                                    <img src="<?= \Yii::getAlias('@web/images/avatar-ribaldinho.jpg') ?>" alt="" class="rounded-circle img-fluid" />
                                    <h4 class="mt-3 my-2 text-capitalize">Ribaldhino </h4>
                                    <p class="mb-2">Autor de "WRap, Trap Y Poesía"</p>
                            </div>
                            <div class="col-md-4 mt-md-0 mt-4">
                                    <img src="<?= \Yii::getAlias('@web/images/avatar-zeux.jpg') ?>" alt="" class="rounded-circle img-fluid" />
                                    <h4 class="mt-3 my-2 text-capitalize">ZeuxisMaster </h4>
                                    <p class="mb-2">Autor de "Protestantismo Vs Catolicismo"</p>
                            </div>
                            <div class="col-md-4 mt-md-0 mt-4">
                                    <img src="<?= \Yii::getAlias('@web/images/avatar-mollychan.jpg') ?>" alt="" class="rounded-circle img-fluid" />
                                    <h4 class="mt-3 my-2 text-capitalize">MollyChan </h4>
                                    <p class="mb-2">Autora de "Un hecho: Crisis Cíclicas"</p>
                            </div>
                    </div>	
	</div>
</section>
