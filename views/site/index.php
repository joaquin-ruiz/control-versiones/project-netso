<?php  
include \Yii::getAlias('@app/views/layouts/headers.php');

?>
<!--Slider-->
<div class="slider">
	<div class="callbacks_container">
		<ul class="rslides" id="slider3">
                    
			<div id="text-carousel" class="carousel slider slider-img1" data-ride="carousel">
                             <!-- <ol class="carousel-indicators">
                                    <li data-target="#my-text-carousel" data-slide-to="0" class="active"></li>
                                    <li data-target="#my-text-carousel" data-slide-to="1"></li>
                                    <li data-target="#my-text-carousel" data-slide-to="2"></li>
                              </ol>-->
                            <div class="carousel-inner slider_banner_info_top">
                                <!-- comprobar class="d-block w-100" -->
                                <div class="item active">
                                    <img  src="<?= \Yii::getAlias('@web/images/primera-sentencia.png') ?>" alt="Primera frase">
                                </div>
                                <div class="item">
                                    <img  src="<?= \Yii::getAlias('@web/images/segunda-sentencia.png') ?>" alt="Second slide">
                                </div>
                                <div class="item">
                                    <img src="<?= \Yii::getAlias('@web/images/tercera-sentencia.png') ?>" alt="Third slide">
                                </div>
                            </div>
                           <!--
                            <a class="left carousel-control" href="#my-text-carousel" data-slide="prev">
                                <span class="fa fa-arrow-left"></span>
                                <span class="sr-only">Anterior</span>
                            </a>
                            <a class="right carousel-control" href="#my-text-carousel" data-slide="next">
                                <span class="fa fa-arrow-right"></span>
                                <span class="sr-only">Siguiente</span>
                            </a>-->
                         </div>
			<li>
        <section class="stats_test pb-5 container-fluid">
                <div class="row inner_stat_wthree_agileits">
                        <div class="col-sm-3 col-6 py-5 stats_left counter_grid">
                                <i class="far fa-edit"></i>
                                <p class="counter">145</p>
                                <h4>Textos</h4>
                        </div>
                        <div class="col-sm-3 col-6 py-5 stats_left counter_grid1">
                                <i class="fas fa-video"></i>
                                <p class="counter">165</p>
                                <h4>Videos</h4>
                        </div>
                        <div class="col-sm-3 col-6 py-5 stats_left counter_grid2">
                                <i class="fas fa-music"></i>
                                <p class="counter">563</p>
                                <h4>Audios</h4>
                        </div>
                        <div class="col-sm-3 col-6 py-5 stats_left counter_grid3">
                                <i class="fas fa-at"></i>
                                <p class="counter">1045</p>
                                <h4>Otros</h4>
                        </div>
                </div>
        </section>
				<div class="slider-img2">
					<div class="dot">
						<div class="container">
							<div class="slider_banner_info_w3ls text-center">
								<p class="h1 home">Somos una plataforma web destinada a fomentar el saber personal y la libertad de pensamiento. 
                                                                    Queremos convertirnos en un apoyo virtual a la cultura universal y a la no censura, como respuesta ante el panorama de extremismos y polarización que la vida política ha instaurado.</p>
							</div>
						</div>
					</div>
				</div>
			</li>
			
        <div class="container">
            <div class="row justify-content-md-center">
              <div class="col col-lg-2">
                <div class="row justify-content-md-center mt-3">
                      <i class="far fa-comments fa-10x"></i>.
                  </div>
                  <div class="row justify-content-md-center mt-3">
                      <h5 class="h4">Debate sin censura</h5>
                      <p>Aquí no hay adscripción ideológica ni partidista. Ni comunista ni fascista, sino reportista. Conozca y rebata las opiniones de una esfera u otra y participe en los maravillosos duelos de la dialéctica.</p>
                  </div>
              </div>
              <div class="col col-lg-3">
                <!-- Blank Column -->
              </div>
              <div class="col col-lg-2">
                  <div class="row justify-content-md-center mt-3">
                      <i class="fas fa-bullhorn fa-10x"></i>.
                  </div>
                  <div class="row justify-content-md-center mt-3">
                      <h5 class="h4">Compártelo con el mundo</h5>
                      <p>Facebook, Twitter y hasta Instagram. Estamos conectados con todas las redes sociales del planeta para que pueda compartir aquello que vea interesante o lo que usted mismo aporte.</p>
                  </div>
              </div>
            </div>
                        <div class="row justify-content-md-center">
              <div class="col col-lg-2">
                <div class="row justify-content-md-center mt-3">
                      <i class="fas fa-university fa-10x"></i>.
                  </div>
                  <div class="row justify-content-md-center mt-3">
                      <h5 class="h4">Contenido diverso</h5>
                      <p>Disponemos de escritos, contenidos audivisuales de las más variopintas características. Crítica desde la perspectiva artística, desde la académica y opiniones bajo el prisma de multitud de sistemas filosóficos. Todo apoyado en datos y pruebas rigurosos.</p>
                  </div>
              </div>
              <div class="col col-lg-3">
                <!-- Blank Column -->
              </div>
              <div class="col col-lg-2">
                <div class="row justify-content-md-center mt-3">
                      <i class="fas fa-question fa-10x"></i>.
                  </div>
                  <div class="row justify-content-md-center mt-3">
                      <h5 class="h4">Totalmente anónimo</h5>
                      <p>Apoyamos la libertad de expresión y todo derecho humano reconocido universalmente. Podrá mantenerse en el anonimato o exponerse si así lo desea.</p>
                  </div>
              </div>
            </div>
        </div>
                        
		</ul>
	</div>
	<div class="clearfix"></div>
</div>
<!--//Slider-->

<!-- About us -->
<section class="about py-5">
	<div class="container">
		<h3 class="heading text-center text-uppercase mb-5"> Artículos de la Web </h3>
				
		<div class="row about_grids mt-5">
			<div class="col-md-4">                            
				<img src="<?= \Yii::getAlias('@web/images/trap-rap-poesia.png') ?>" alt="" class="img-fluid fit-image" />
				<h3 class="mt-3 my-2 text-capitalize">Rap, Trap y Poesía </h3>
				<p class="mb-2">La primera pregunta es obvia y necesaria: ¿qué es el trap? Para Yung Beef, «el trap es cocaína y follar». </p>
				<a href="#" class="text-capitalize">Read more <span class="fas fa-long-arrow-alt-right"></span></a>
			</div>
			<div class="col-md-4 mt-md-0 mt-4">                            
				<img src="<?= \Yii::getAlias('@web/images/protestantismo-vs-catolicismo.jpg') ?>" alt="" class="img-fluid fit-image" />
				<h3 class="mt-3 my-2 text-capitalize">Protestantismo vs Catolicismo </h3>
				<p class="mb-2">Los protestantes y los católicos son dos ramas distintas del cristianismo y que no son iguales. Pero, ¿cuáles son las diferencias fundamentales entre un católico y un protestante?</p>
				<a href="#" class="text-capitalize">Read more <span class="fas fa-long-arrow-alt-right"></span></a>
			</div>
			<div class="col-md-4 mt-md-0 mt-4">
				<img src="<?= \Yii::getAlias('@web/images/crisis-ciclicas.png') ?>" alt="" class="img-fluid fit-image" />
				<h3 class="mt-3 my-2 text-capitalize">Un hecho: Crisis Cíclicas </h3>
				<p class="mb-2">Uno de los mayores problemas de los que adolecen nuestros juicios económicos es que tratamos de elucubrarlos a la luz de nuestra experiencia diaria. </p>
				<a href="#" class="text-capitalize">Read more <span class="fas fa-long-arrow-alt-right"></span></a>
			</div>
		</div>
		
	</div>
</section>
<!-- //About us -->

<!-- about bottom -->
<section class="aboutbottom-grids pt-5 container-fluid">
	<div class="row slider-img2">
		<div class="col-lg-6 aboutleft">
			<img src="<?= \Yii::getAlias('@web/images/about1.jpg') ?>" alt="" class="img-fluid"/>
		</div>
		<div class="col-lg-6 p-sm-5 p-3 aboutright">
			<h3 class="mb-3">Contenido Interesante</h3>
			<p class="mb-2">Un muy interesante conversatorio de 55 minutos de duración de la mano de INAH TV. Viajen de la mano de la antropóloga e historiadora Lourdes Márquez Morfin y la Dra.América Molina del Villar a través de las pandemias que han sucedido a lo largo de la historia. </p>
			<iframe src="https://www.youtube.com/embed/RuHvXHWOczQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
	</div>
</section>
<!-- //about bottom -->

<section class="work py-5">
	<div class="container">
	<h3 class="heading text-center text-uppercase mb-5 "> Cómo trabajamos </h3>
		<div class="row">
			<div class="col-lg-3 col-sm-6 grid1">
				<!--<img src=""<?= \Yii::getAlias('@web/images/1.png') ?>" alt="" class="img-fluid mb-3">-->
				<h3 class="my-2">Contenido libre</h3>
				<p class="">Los trabajos de nuestros usuarios no tienen limitación, tratan cualquier tema posible bajo las premisas de la tolerancia y la no incitación al odio.</p>
				<p></p>
			</div>
                        <div class="col-1"></div>
			<div class="col-lg-3 col-sm-6 grid1">
				<!--<img src="<?= \Yii::getAlias('@web/images/2.png') ?>" alt="" class="img-fluid mb-3">-->
				<h3 class="my-2">Todos los formatos</h3>
				<p class="">Textos, videos, audios, imágenes y hasta crucigramas. ¡La imaginación no tiene límites! Atrévete con tus propias creaciones y sorpréndenos con tu originalidad. </p>
				<p></p>
			</div>
                        <div class="col-1"></div>
			<div class="col-lg-3 col-sm-6 mt-lg-0 mt-sm-5 grid1">
				<!--<img src="<?= \Yii::getAlias('@web/images/3.png') ?>" alt="" class="img-fluid mb-3">-->
				<h3 class="my-2">Revisión paulatina</h3>
				<p class="">Nuestro equipo de moderadores va revisando progresivamente el contenido, marcando su actualidad y comprobando que no infrinja ninguna de nuestras normas básica.</p>
				<p></p>
			</div>
			<!--<div class="col-lg-3 col-sm-6 mt-lg-0 mt-sm-5 grid2">
				<img src="images/aa.jpg" alt="" class="img-fluid">
			</div>-->
		</div>
	</div>
</section>
<!-- //How we Work -->


<!--Team-->
<section class="Team py-5 slider-img2">
	<div class="container">
	<h3 class="heading text-center text-uppercase mb-5 heading-color"> Nuestros Mecenas </h3>
		<div class="row inner-sec-w3layouts-agileinfo">
			<div class="col-md-3 col-sm-6 team-grids">
				<img src="<?= \Yii::getAlias('@web/images/bill gates.jpg') ?>" class="img-responsive" alt="">
				<div class="team-info">
					<div class="caption">
						<h4>Bill Gates</h4>
						<h6>Colaborador Estrella</h6>
					</div>
					<div class="social-icons-section">
						<a class="fac" href="#">
							<i class="fab fa-facebook"></i>
						</a>
						<a class="twitter" href="#">
							<i class="fab fa-twitter-square"></i>
						</a>
						<a class="pinterest" href="#">
							<i class="fab fa-pinterest"></i>
						</a>

					</div>

				</div>
			</div>
			<div class="col-md-3 col-sm-6 team-grids">
				<img src="<?= \Yii::getAlias('@web/images/mark_zukerberg.jpg') ?>" class="img-responsive" alt="">
				<div class="team-info">
					<div class="caption">
						<h4>Mark Zuckerberg</h4>
						<h6>Colaborador Estrella</h6>
					</div>
					<div class="social-icons-section">
						<a class="fac" href="#">
							<i class="fab fa-facebook"></i>
						</a>
						<a class="twitter" href="#">
							<i class="fab fa-twitter-square"></i>
						</a>
						<a class="pinterest" href="#">
							<i class="fab fa-pinterest"></i>
						</a>

					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 team-grids">
				<img src="<?= \Yii::getAlias('@web/images/debra nirboum.jpg') ?>" class="img-responsive" alt="">
				<div class="team-info">
					<div class="caption">
						<h4>Debra Nirboum</h4>
						<h6>Colaborador Estrella</h6>
					</div>
					<div class="social-icons-section">
						<a class="fac" href="#">
							<i class="fab fa-facebook"></i>
						</a>
						<a class="twitter" href="#">
							<i class="fab fa-twitter-square"></i>
						</a>
						<a class="pinterest" href="#">
							<i class="fab fa-pinterest"></i>
						</a>

					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 team-grids">
				<img src="<?= \Yii::getAlias('@web/images/amancio ortega.jpg') ?>" class="img-responsive" alt="">
				<div class="team-info">
					<div class="caption">
						<h4>Amancio Ortega </h4>
						<h6>Colaborador Estrella</h6>
					</div>
					<div class="social-icons-section">
						<a class="fac" href="#">
							<i class="fab fa-facebook"></i>
						</a>
						<a class="twitter" href="#">
							<i class="fab fa-twitter-square"></i>
						</a>
						<a class="pinterest" href="#">
							<i class="fab fa-pinterest"></i>
						</a>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--//Team-->

<!-- Newsletter -->
<section class="newsletter text-center py-5">
	<div class="container">
		<h3 class="heading text-center text-uppercase mb-5"> Boletín </h3>
		
		<div class="subscribe_inner">
			<p class="mb-4">Únete a más de 1000 escritores, artistas y librepensadores que reciben información y actualidad de nuestra comunidad</p>
			<form action="#" method="post">
				<input type="email" placeholder="Introducir dirección de e-mail" required="">
				<input type="submit" class="mt-3" value="Suscribirse">
			</form>			
		</div>
		
	</div>
</section>
<!-- Vertically centered Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-uppercase" id="exampleModalLongTitle">Cafe In</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<img src="images/4.jpg" class="img-fluid mb-3" alt="cafe Image" />
        Vivamus eget est in odio tempor interdum. Mauris maximus fermentum arcu, ac finibus ante. Sed mattis risus at ipsum elementum, ut auctor turpis cursus. Sed sed odio pharetra, aliquet velit cursus, vehicula enim. Mauris porta aliquet magna, eget laoreet ligula.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- //Vertically centered Modal -->


<!-- js-scripts -->		

	<!-- js -->
	<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script> <!-- Necessary-JavaScript-File-For-Bootstrap --> 
	<!-- //js -->	
	
	<!-- Banner Responsive slider -->
	<script src="js/responsiveslides.min.js"></script>
	<script>
            
		// You can also use "$(window).load(function() {"
		$(function () {
			// Slideshow 3
			$("#slider3").responsiveSlides({
				auto: true,
				pager: false,
				nav: true,
				speed: 500,
				namespace: "callbacks",
				before: function () {
					$('.events').append("<li>before event fired.</li>");
				},
				after: function () {
					$('.events').append("<li>after event fired.</li>");
				}
			});

		});
	</script>
	<!-- // Banner Responsive slider -->
	
	<!-- stats -->
	<script src="js/jquery.waypoints.min.js"></script>
	<script src="js/jquery.countup.js"></script>
	<script>
		$('.counter').countUp();
	</script>
	<!-- //stats -->
	
	<!-- search-bar -->
	<script src="js/main.js"></script>
	<!-- //search-bar -->
	
	<!-- start-smoth-scrolling -->
	<script src="js/SmoothScroll.min.js"></script>
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
	</script>
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
	<!-- //here ends scrolling icon -->
	<!-- start-smoth-scrolling -->

	<!-- jQuery-Photo-filter-lightbox-Gallery-plugin -->
	<script type="text/javascript" src="js/jquery-1.7.2.js"></script>
	<script src="js/jquery.quicksand.js" type="text/javascript"></script>
	<script src="js/script.js" type="text/javascript"></script>
	<script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
	<!-- //jQuery-Photo-filter-lightbox-Gallery-plugin -->

<!-- //js-scripts -->

</body>
</html>