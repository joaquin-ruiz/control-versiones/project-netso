<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */


use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Regístrate';
include \Yii::getAlias('@app/views/layouts/headers.php');

?>
<div class="simple-vcenter-slider contact-slider">
    <div class="container">
           <div class="d-flex justify-content-center">
                   <div class="card">
                           <div class="card-header">
                                   <h3>Regístrate</h3>
                                 
                                   <div class="d-flex justify-content-end social_icon">
                                           <span><i class="fab fa-facebook-square"></i></span>
                                           <span><i class="fab fa-google-plus-square"></i></span>
                                           <span><i class="fab fa-twitter-square"></i></span>
                                   </div>
                           </div>
                           <div class="card-body">
                                    <?php $form = ActiveForm::begin([
                                        'method' => 'post',
                                        'id' => 'formulario',
                                        'enableClientValidation' => false,
                                        'enableAjaxValidation' => true,
                                       ]);
                                    ?>
                                           <div class="input-group form-group">
                                                   <div class="input-group-prepend">
                                                           <span class="input-group-text form-spans"><i class="fas fa-user"></i></span>
                                                   </div>
                                                <input type="text" id="formregister-username" class="form-control" name="FormRegister[username]" placeholder="Usuario" aria-required="true">
                                                <!-- form->field($model, "username", ['options' => ['class' => 'form-group']])->input("text") -->

                                           </div>
                                           <div class="input-group form-group">
                                                   <div class="input-group-prepend">
                                                           <span class="input-group-text form-spans"><i class="fas fa-envelope"></i></span>
                                                   </div>
                                               <input type="email" id="formregister-email" class="form-control" name="FormRegister[email]" placeholder="Correo electrónico" aria-required="true">
                                                   <!-- $form->field($model, "email")->input("email") -->

                                           </div>
                                           <div class="input-group form-group">
                                                   <div class="input-group-prepend">
                                                           <span class="input-group-text form-spans"><i class="fas fa-key"></i></span>
                                                   </div>
                                               <input type="password" id="formregister-password" class="form-control" name="FormRegister[password]" placeholder="Contraseña" aria-required="true">
                                                  <!-- $form->field($model, "password")->input("password") -->

                                           </div>
                                           <div class="input-group form-group">
                                                   <div class="input-group-prepend">
                                                           <span class="input-group-text form-spans"><i class="fas fa-key"></i></span>
                                                   </div>
                                               <input type="password" id="formregister-password_repeat" class="form-control" name="FormRegister[password_repeat]" placeholder="Repite contraseña" aria-required="true">
                                                  <!-- $form->field($model, "password_repeat")->input("password") -->

                                           </div>   
                                           <div class="form-group">
                                               <?= Html::submitButton("Register", ["class" => "btn btn-primary float-right"]) ?>
                                                   <!--<input type="submit" value="Registrar" class="btn float-right btn-login">-->
                                           </div>
                                   <?php $form->end() ?>
                           </div>
           </div>
       </div>
   </div>
</div>
