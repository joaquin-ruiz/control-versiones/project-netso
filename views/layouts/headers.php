<!--
	Author: W3layouts
	Author URL: http://w3layouts.com
	License: Creative Commons Attribution 3.0 Unported
	License URL: http://creativecommons.org/licenses/by/3.0/
-->

<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;


 $url = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
?>

<!DOCTYPE html>
<html lang="es">
<head>
<title>NetSo</title>
	
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<meta name="keywords" content="Netso página web dedicada a la libertad de expresión y pensamiento." />
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--// Meta tag Keywords -->	
	
	<!-- web-fonts -->
	<link href="//fonts.googleapis.com/css?family=Alegreya+Sans:100,100i,300,300i,400,400i,500,500i,700,700i,800,800i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
	<!-- //web-fonts -->
	
</head>

<body>
<!-- Navigation -->
<header>
	<div class="top-nav">
		<!--<div class="container">-->
			<nav class="navbar navbar-expand-lg navbar-light">
                            <a href="<?= Url::toRoute('site/index');?>" title="Vuelta al inicio" id="logo">
                                <img src="<?= \Yii::getAlias('@web/images/logotipo-netso.png') ?>" alt="NetSo logo" />
                            </a>                            
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse justify-content-center pr-md-4" id="navbarSupportedContent">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item">
							<a class="nav-link" href="<?= Url::toRoute('site/index');?>">Inicio <span class="sr-only">(current)</span></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?= Url::toRoute('site/about');?>" >Conócenos</a>-
						</li>
                                               
						<li class="nav-item">
                                                    
							<a href="<?= Url::toRoute('site/contact');?>" class="nav-link">Contacto</a>							
						</li>
                                                <li class="nav-item">
                                                <?php
                                                if (Yii::$app->user->isGuest){                                                    
                                                   /* Html::tag('a', ['/site/index'], ['class' => ['nav-link']]); */
                                                ?>
                                                <a class="nav-link" href="<?= Url::toRoute('site/register');?>">Registro</a>
                                                <?php
                                                } else {
                                                ?>
                                                     <a class="nav-link" href="<?= Url::toRoute('site/login');?>">Perfil</a>
                                                <?php
                                                 } ?>
                                                </li>
                                                <li class="nav-item">
							<a href="<?= Url::toRoute('works/blog');?>" class="nav-link">Blog</a>							
						</li>
                                                <ul class="nav navbar-nav navbar-right">
                                                    <li class="dropdown">
                                                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Lenguaje <span class="caret"></span></a>
                                                        
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li>
                                                                <?= Html::a(Html::img('@web/images/es_ES.png', ['class' => 'language']),['site/login']); ?>
                                                            </li>
                                                                <!--<a href="#" class="language" rel="it-IT"><img src=" \Yii::getAlias('@web/images/es_ES.png') " alt="Italiano" /></a>-->
                                                            <li>
                                                                <?= Html::a(Html::img('@web/images/en_US.png', ['class' => 'language']),['site/login']); ?>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>    
					</ul>
				</div>
			</nav>
		<!--</div>-->
	</div>
</header>

