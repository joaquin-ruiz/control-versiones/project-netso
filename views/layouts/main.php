<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;        

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php    
    $this->registerJsFile(
    '@web/js/jquery-3.5.1.min.js',
    );
     $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    
    ?>

    <!--<div class="container">-->
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    <!--</div>-->
</div>

<footer class="footer">
    <div class="container">
       
            <div class="col-lg-5" >
                <a href="<?= Url::toRoute('site/index');?>" title="Vuelta al inicio" id="logo">
                    <img src="<?= \Yii::getAlias('@web/images/logotipo-netso.png') ?>" class="img-fluid footer-logo" alt="NetSo logo" />
                </a> 
                <p class="pull-left">&copy; Compañía NetSo <?= date('Y') ?>.<small>Todos los derechos reservados.</small></p>
            </div>
            <div class="col-lg-3" ></div>           
           <div class="col-lg-2" >
               <p><u>Mapa del Sitio</u></p>
                            <ul class="vlist">
                                <li><a href="index.html">Inicio</a></li><br>
                                <li><a href="services.html">Conócenos</a></li><br>
                                <li><a href="gallery.html">Contacto</a></li><br>
                                <li><a href="error.html">Blog</a></li><br>
                                <li><a href="contact.html">Registro</a></li>
                            </ul>
            </div>
            <div class="col-lg-2 justify-content-center">
               <p><u>Síguenos</u></p>
                            <ul class="d-flex mt-3">
                                <li><a href="#"><span class="fab fa-facebook"></span></a></li><br>
                                <li><a href="#"><span class="fab fa-twitter-square"></span></a></li><br>
                                <li><a href="#"><span class="fab fa-instagram"></span></a></li><br>
                                <li><a href="#"><span class="fab fa-linkedin"></span></a></li><br>
                                <li><a href="#"><span class="fab fa-google-plus-square"></span></a></li>
                            </ul>
            </div>            
    </div>
</footer>
 <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
 <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>-->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
