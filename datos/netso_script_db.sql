﻿DROP DATABASE IF EXISTS netso;

CREATE DATABASE netso
CHARACTER SET utf8
COLLATE utf8_general_ci;

USE netso;

/* TABLES */

CREATE OR REPLACE TABLE users (
  id int(9) AUTO_INCREMENT, 
  username varchar(12) NOT NULL,
  password varchar(254) NOT NULL,
  email varchar(254) NOT NULL,
  register_date TIMESTAMP NOT NULL,
  authKey varchar(250) NOT NULL,
  accessToken varchar(250) NOT NULL,
  activate tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (id)
  ) ENGINE=InnoDB; 

CREATE OR REPLACE TABLE works (
  id int(9) AUTO_INCREMENT,
  u_id int(9) NOT NULL,
  title varchar(254) NOT NULL,
  content TEXT(10) NOT NULL,
  no_public bool DEFAULT TRUE,
  content_type int(9) NOT NULL,
  categories enum(""),
  PRIMARY KEY (id)
  ) ENGINE=InnoDB; 
  
  CREATE OR REPLACE TABLE works_has_categories (
  id int(9) AUTO_INCREMENT,
  w_id int(9) NOT NULL,
  c_id int(9) NOT NULL,
  PRIMARY KEY (id)
  ) ENGINE=InnoDB; 

  
  CREATE OR REPLACE TABLE categories (
  id int(9) AUTO_INCREMENT,
  name varchar(254) NOT NULL,
  PRIMARY KEY (id)
  ) ENGINE=InnoDB; 


CREATE OR REPLACE TABLE content_types (
  id int(9) AUTO_INCREMENT,
  name varchar(254) NOT NULL,
  PRIMARY KEY (id)
  ) ENGINE=InnoDB;

/* CONSTRAINTS */
 ALTER TABLE works 
    ADD CONSTRAINT fk_works_type
    FOREIGN KEY (content_type) 
    REFERENCES content_types(id),
   /* ADD CONSTRAINT fk_works_user
    FOREIGN KEY (u_id) 
    REFERENCES users(id)*/
    ON DELETE CASCADE ON UPDATE CASCADE;

