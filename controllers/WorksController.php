<?php

namespace app\controllers;

use Yii;
use app\models\Ciclista;
use app\models\Works;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CiclistaController implements the CRUD actions for Ciclista model.
 */
class WorksController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ciclista models.
     * @return mixed
     */
    public function actionBlog()
    {
        
        $title = Works::find()->select('title')->all();
        $content = Works::find()->select('content')->all();
        $id = Works::find()->select('id')->all();
        $dataProvider = new ActiveDataProvider([
            'query' => Works::find(),
            
        ]);

        return $this->render('blog', [
            'dataProvider' => $dataProvider,
            'title' => $title,
            'content' => $content,
            'id' => $id,
            
        ]);
    }

    /**
     * Displays a single Ciclista model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
  /*  public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }*/
    
    public function actionView($id) {
        $id = (int) $id;
        $work = Works::find()->where(['id' => $id])->one();
        
        if (!$work) {
            throw new NotFoundHttpException(Yii::t('app', 'The post not found'));
        }
        
      /*  if ($work->status != Constant::STATUS_PUBLISHED && (!Helper::isOwner($work->created_by) && !Helper::isAdmin())) {
            throw new NotFoundHttpException(Yii::t('app', 'The post is not published.'));
        }
        elseif ($work->status == Constant::STATUS_PUBLISHED && $work->published_time > time() && (!Helper::isOwner($work->created_by) && !Helper::isAdmin())) {
            throw new NotFoundHttpException(Yii::t('app', 'The post is not published.'));
        }*/

        return $this->render('view', [
                    'model' => $work,
                    'id' => $id,
        ]);
    }

    /**
     * Creates a new Ciclista model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new \app\models\Works();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ciclista model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
       

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'ID' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'id' => $model->id,
        ]);
    }

    /**
     * Deletes an existing Ciclista model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ciclista model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ciclista the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Works::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
  
}