<?php

namespace app\models;

use Yii;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "ciclista".
 *
 * @property int $dorsal
 * @property string $nombre
 * @property int|null $edad
 * @property string $nomequipo
 *
 * @property Equipo $nomequipo0
 * @property Etapa[] $etapas
 * @property Lleva[] $llevas
 * @property Puerto[] $puertos
 */
class Works extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'works';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'content'], 'required'],
            [['u_id'], 'integer'],
            [['title'], 'string',],
            [['id'], 'unique'],
           
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id'),
            'u_id' => Yii::t('app', 'Id de usuario'),
            'title' => Yii::t('app', 'Título'),
        ];
    }

    /**
     * Gets query for [[Nomequipo0]].
     *
     * @return \yii\db\ActiveQuery
     */
    /**
     * Gets query for [[Etapas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getU_id()
    {
        return $this->hasMany(Users::className(), ['u_id' => 'id']);
    }

    /**
     * Gets query for [[Llevas]].
     *
     * @return \yii\db\ActiveQuery
     */

    /**
     * Gets query for [[Puertos]].
     *
     * @return \yii\db\ActiveQuery
     */

}
