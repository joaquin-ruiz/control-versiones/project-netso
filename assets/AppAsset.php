<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/bootstrap.css',        
        'css/login.css',
        'css/fontawesome-all.css',
        'css/prettyPhoto.css',
        'css/style.css',
                
    ];
    public $js = [
        'js/jquery-3.5.1.min.js',
        'js/bootstrap.js',
        'js/owl.carousel.js', 
        'js/responsiveslides.min.js',
        'js/jquery.waypoints.min.js',
        'js/jquery.countup.js',
        'js/main.js',
        'js/SmoothScroll.min.js',
        'js/move-top.js',
        'js/easing.js',     
        'js/jquery.quicksand.js',
        'js/script.js',
        'js/jquery.prettyPhoto.js',
        
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
